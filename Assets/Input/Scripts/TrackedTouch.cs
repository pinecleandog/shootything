﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Input
{
    public enum TouchPhase
    {
        Began = 0,
        Ended
    }

    public class TrackedTouch
    {       
        public TrackedTouch()
        {
            FingerId = -1;
            Position = Vector2.zero;
            TouchPhase = TouchPhase.Ended;
        }

        public int FingerId { get; set; }

        public Vector2 Position { get; set; }

        public TouchPhase TouchPhase { get; set; }
    }
}