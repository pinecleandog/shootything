﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Input
{
    /// <summary>
    /// A generic very interface to simulate touch input on desktop. 
    /// Will only track one "touch" at a time.
    /// </summary>
    public class TouchInputManager : MonoBehaviour
    {
        // only one tracked touch for now
        private TrackedTouch trackedTouch = new TrackedTouch();

        private List<ITouchHandler> touchHandlers = new List<ITouchHandler>();

        public void AddTouchHandler(ITouchHandler handler)
        {
            if(!touchHandlers.Contains(handler))
            {
                touchHandlers.Add(handler);
            }
        }

        public void RemoveTouchHandler(ITouchHandler handler)
        {
            touchHandlers.Remove(handler);
        }

        public void Update()
        {
            bool mouseDown = UnityEngine.Input.GetMouseButton(0);

            if (trackedTouch.TouchPhase == TouchPhase.Ended)
            {
                if (mouseDown)
                {
                    trackedTouch.TouchPhase = TouchPhase.Began;
                    trackedTouch.FingerId = 0;
                    trackedTouch.Position = UnityEngine.Input.mousePosition;

                    foreach (var handler in touchHandlers)
                    {
                        handler.OnTouchBegin(trackedTouch);
                    }
                }
            }
            else
            {
                trackedTouch.Position = UnityEngine.Input.mousePosition;

                if (!mouseDown)
                {
                    trackedTouch.TouchPhase = TouchPhase.Ended;
                    foreach (var handler in touchHandlers)
                    {
                        handler.OnTouchEnd(trackedTouch);
                    }
                }
            }
        }
    }
}