﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Input
{
    public interface ITouchHandler
    {
        void OnTouchBegin(TrackedTouch touch);
        void OnTouchEnd(TrackedTouch touch);
    }
}

