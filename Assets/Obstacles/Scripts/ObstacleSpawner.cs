﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;

namespace Obstacles
{
    public class ObstacleSpawner : Spawner<Obstacle>
    {
        [SerializeField, Tooltip("Y position on the screen to spawn obstacles. "+
            "Negative values will be off the top of the screen")]
        private float spawnPointY = -10f;

        [SerializeField, Tooltip("Speed of projectiles in units per second")]
        private float obstacleSpeed = 5f;

        [SerializeField, Tooltip("Percentage of the screen width, from the centre, that obstacles "+
            "can be spawned in. eg: 80 = the middle 80%")]
        private float spawnableArea = 80;

        [SerializeField, Tooltip("A curve where X is time passed, and Y is how many obstacles "+
            "spawn per second")]
        private AnimationCurve spawnRateCurve;

        [SerializeField, Tooltip("Maps Y shots required for Y scale of an obstacle")]
        private AnimationCurve hitsToSizeRatio;

        [SerializeField, Tooltip("Maps Y scale for X seconds passed, Minimum possible scale")]
        private AnimationCurve objectMinSizeCurve;

        [SerializeField, Tooltip("Maps Y scale for X seconds passed, Maximum possible scale")]
        private AnimationCurve objectMaxSizeCurve;

        [SerializeField]
        private Transform ship;

        [SerializeField]
        private GameCamera gameCamera;

        public override void Awake()
        {
            base.Awake();

            if (gameCamera == null)
            {
                Debug.LogError("No GameCamera set on ObstacleManager");
                enabled = false;
            }
            if(ship == null)
            {
                Debug.LogError("No ship set on ObstacleManager");
            }

            if (!enabled) return;

            onObjectSpawn += onObjectSpawned;

            populatePool(Mathf.CeilToInt(objectSpawnRate * objectLifeTime) + 1);
        }

        public void OnEnable()
        {
            GlobalEvents.AddListener<Game.Event.GameEnded>(OnGameEnded);
        }

        public void OnDisable()
        {
            GlobalEvents.RemoveListener<Game.Event.GameEnded>(OnGameEnded);
        }

        public override void Update()
        {
            base.Update();

            objectSpawnRate = Mathf.RoundToInt(spawnRateCurve.Evaluate(Time.timeSinceLevelLoad));
        }

        private void onObjectSpawned(Obstacle obj)
        {
            obj.transform.rotation = Quaternion.LookRotation(-Vector3.forward);

            int noSpawnMargin = Mathf.RoundToInt(Screen.width * (spawnableArea / 100));

            var spawnPosScrn = new Vector2(
                Random.Range(noSpawnMargin, Screen.width - noSpawnMargin),
                Screen.height + spawnPointY);

            Vector3 newPos = gameCamera.screenToWorldPoint(spawnPosScrn);
            newPos.y = ship.transform.position.y;

            float minScale = objectMinSizeCurve.Evaluate(Time.timeSinceLevelLoad);
            float maxScale = objectMaxSizeCurve.Evaluate(Time.timeSinceLevelLoad);
            float scale = Random.Range(minScale, maxScale);
            obj.transform.localScale = Vector3.one * scale;

            obj.transform.position = newPos;
            obj.Speed = obstacleSpeed;
            obj.HitPoints = Mathf.FloorToInt(hitsToSizeRatio.Evaluate(scale));
        }

        #region Event Handlers

        private void OnGameEnded(Game.Event.GameEnded ev)
        {
            doSpawn = false;
        }

        #endregion

    }

}
