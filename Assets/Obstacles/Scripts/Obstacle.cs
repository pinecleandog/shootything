﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;

public class Obstacle : MonoBehaviour, ISpawnable{

    public float Speed { get; set; }
    public int HitPoints { get; set; }

    private Rigidbody rigidBody;

    public void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
        if (rigidBody == null)
        {
            Debug.LogError("RigidBody attached to Obstacle");
        }
    }

    public void OnTriggerEnter(Collider collider)
    {
        if(collider.CompareTag("Projectile"))
        {
            var projectile = collider.GetComponent<Ship.Projectile>();
            if(projectile == null)
            {
                Debug.LogError("No Projectile Component was found on projectile");
                return;
            }

            (projectile as ISpawnable).Destroy();

            HitPoints--;
            if(HitPoints <= 0)
            {
                (this as ISpawnable).Destroy();
            }
        }
    }

#region ISpawnable Implementation

    Transform ISpawnable.Transform
    {
        get
        {
            return transform;
        }
    }

    float ISpawnable.ExpireyTime { get; set; }
    ISpawner ISpawnable.Spawner { get; set; }

    void ISpawnable.Disable()
    {
        gameObject.SetActive(false);
    }

    void ISpawnable.Enable()
    {
        if (rigidBody == null) return;

        gameObject.SetActive(true);
        rigidBody.velocity = transform.forward * Speed;
    }

    void ISpawnable.Destroy()
    {
        var spawnable = this as ISpawnable;

        var ev = GlobalEvents.CreateEventObject<Game.Event.ObstacleDestroyed>();
        ev.Obstacle = this;
        GlobalEvents.Trigger(ev);

        if (spawnable.Spawner == null)
        {
            DestroyImmediate(this.gameObject);
            return;
        }

        spawnable.Spawner.DestroyObject(spawnable);
    }


#endregion
}
