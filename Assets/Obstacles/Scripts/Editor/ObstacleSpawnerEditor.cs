﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Obstacles.ObstacleSpawner))]
public class ObstacleSpawnerEditor : Editor
{
    SerializedProperty prefab,
        lifeTime,
        spawnPointY,
        obstacleSpeed,
        spawnableArea,
        spawnRateCurve,
        objectMinSizeCurve,
        objectMaxSizeCurve,
        hitsToSizeRatio,
        ship,
        gameCamera;
	
    public void OnEnable()
    {
        prefab = serializedObject.FindProperty("prefab");
        lifeTime = serializedObject.FindProperty("objectLifeTime");
        spawnPointY = serializedObject.FindProperty("spawnPointY");
        obstacleSpeed = serializedObject.FindProperty("obstacleSpeed");
        spawnableArea = serializedObject.FindProperty("spawnableArea");
        spawnRateCurve = serializedObject.FindProperty("spawnRateCurve");
        objectMinSizeCurve = serializedObject.FindProperty("objectMinSizeCurve");
        objectMaxSizeCurve = serializedObject.FindProperty("objectMaxSizeCurve");
        hitsToSizeRatio = serializedObject.FindProperty("hitsToSizeRatio");
        ship = serializedObject.FindProperty("ship");
        gameCamera = serializedObject.FindProperty("gameCamera");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(prefab);
        EditorGUILayout.PropertyField(lifeTime);
        EditorGUILayout.PropertyField(spawnPointY);
        EditorGUILayout.PropertyField(obstacleSpeed);
        EditorGUILayout.PropertyField(spawnableArea);
        EditorGUILayout.PropertyField(spawnRateCurve);
        EditorGUILayout.PropertyField(objectMinSizeCurve);
        EditorGUILayout.PropertyField(objectMaxSizeCurve);
        EditorGUILayout.PropertyField(hitsToSizeRatio);
        EditorGUILayout.PropertyField(ship);
        EditorGUILayout.PropertyField(gameCamera);

        serializedObject.ApplyModifiedProperties();
    }
}
