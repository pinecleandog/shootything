﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;

namespace Ship
{
    public class ShipCollisionHandler : MonoBehaviour
    {
        public void OnTriggerEnter(Collider collider)
        {
            if(collider.CompareTag("Obstacle"))
            {
                var ev = GlobalEvents.CreateEventObject<Game.Event.PlayerShipDestroyed>();
                ev.Ship = transform;
                GlobalEvents.Trigger(ev);

                Destroy(gameObject);
            }
        }
    }

}

