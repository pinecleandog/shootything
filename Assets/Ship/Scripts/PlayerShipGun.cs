﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ship
{
    public class PlayerShipGun : Spawner<Projectile>
    {
        [SerializeField, Tooltip("Speed of projectiles in units per second")]
        private float projectileSpeed = 0.1f;
     
        public override void Awake()
        {
            base.Awake();

            if (!enabled) return;

            onObjectSpawn += onObjectSpawned;

            populatePool(Mathf.CeilToInt(objectSpawnRate * objectLifeTime) + 1);
        }

        private void onObjectSpawned(Projectile obj)
        {
            obj.transform.rotation = Quaternion.LookRotation(transform.forward);
            obj.transform.position = transform.position;
            obj.Speed = projectileSpeed;
        }
      
    }
}

