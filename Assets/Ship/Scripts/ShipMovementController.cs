﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Input
{
    public class ShipMovementController : MonoBehaviour, ITouchHandler
    {
        [SerializeField]
        private TouchInputManager touchManager;

        [SerializeField]
        private GameCamera gameCamera;

        private Rigidbody rigidBody;
        private TrackedTouch trackedTouch = null;
        private Vector2 lastTouchPos;
        
        public void Awake()
        {
            if (touchManager == null)
            {
                Debug.LogError("Touch input manager not set on ScreenViewController");
                enabled = false;
            }
            rigidBody = GetComponent<Rigidbody>();
            if (rigidBody == null)
            {
                Debug.LogError("No RigidBody attached to PlayerShip");
                enabled = false;
            }
            if(gameCamera == null)
            {
                Debug.LogError("Game Camera not set on PlayerShip");
                enabled = false;
            }

            touchManager.AddTouchHandler(this);
        }

        public void FixedUpdate()
        {
            if (trackedTouch == null) return;

            var newPos = gameCamera.screenToWorldPoint(trackedTouch.Position);
            var lastPos = gameCamera.screenToWorldPoint(lastTouchPos);
            Vector3 newShipPos = transform.position + (newPos - lastPos);

            if(gameCamera.PointWithinScreen(newShipPos))
            {
                rigidBody.MovePosition(newShipPos);
            }

            lastTouchPos = trackedTouch.Position;
        }

        #region ITouchHandler Implementation

        void ITouchHandler.OnTouchBegin(TrackedTouch touch)
        {
            if (trackedTouch == null)
            {
                trackedTouch = touch;
                lastTouchPos = touch.Position;
            }
        }

        void ITouchHandler.OnTouchEnd(TrackedTouch touch)
        {
            if (trackedTouch == touch)
            {
                trackedTouch = null;
            }
        }

        #endregion
    }
}
