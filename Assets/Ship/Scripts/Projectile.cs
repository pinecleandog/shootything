﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ship
{
    public class Projectile : MonoBehaviour, ISpawnable
    {
        public float Speed { get; set; }

        private Rigidbody rigidBody;

        public void Awake()
        {
            rigidBody = GetComponent<Rigidbody>();
            if (rigidBody == null)
            {
                Debug.LogError("RigidBody attached to Projectile");
            }
        }

        #region ISpawnable Implementation

        Transform ISpawnable.Transform
        {
            get
            {
                return transform;
            }
        }

        float ISpawnable.ExpireyTime { get; set; }
        ISpawner ISpawnable.Spawner { get; set; }

        void ISpawnable.Destroy()
        {
            var spawnable = this as ISpawnable;

            if (spawnable.Spawner == null)
            {
                DestroyImmediate(this.gameObject);
                return;
            }

            spawnable.Spawner.DestroyObject(spawnable);
        }

        void ISpawnable.Enable()
        {
            if (rigidBody == null) return;

            gameObject.SetActive(true);
            rigidBody.velocity = transform.forward * Speed;
        }

        void ISpawnable.Disable()
        {
            if (!enabled) return;
            gameObject.SetActive(false);
        }

#endregion

    }

}

