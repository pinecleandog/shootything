﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Events;

namespace UI
{
    public class Score : MonoBehaviour
    {
        [SerializeField]
        private Text text;

        private const string scoreTextFormat = "Score: {0}";

        private int destroyedObstacles = 0;

        public void Awake()
        {
            if(text == null)
            {
                Debug.LogError("Text not set on Score component!");
                enabled = false;
            }
        }

        public void OnEnable()
        {
            GlobalEvents.AddListener<Game.Event.ObstacleDestroyed>(OnObstacleDestroyed);
        }

        public void OnDisable()
        {
            GlobalEvents.RemoveListener<Game.Event.ObstacleDestroyed>(OnObstacleDestroyed);
        }

        #region Event Handlers

        public void OnObstacleDestroyed(Game.Event.ObstacleDestroyed ev)
        {
            if (!enabled) return;

            destroyedObstacles++;

            text.text = string.Format(scoreTextFormat, destroyedObstacles);
        }

        #endregion
    }

}

