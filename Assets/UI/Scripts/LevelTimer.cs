﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Events;

namespace UI
{
    public class LevelTimer : MonoBehaviour
    {
        [SerializeField]
        private Text text;

        private const string timerFormat = "Time: {0:0.##}";

        public void Awake()
        {
            if (text == null)
            {
                Debug.LogError("Text not set on LevelTimer component!");
                enabled = false;
            }
        }

        public void OnEnable()
        {
            GlobalEvents.AddListener<Game.Event.GameEnded>(OnGameEnded);
        }

        public void OnDisable()
        {
            GlobalEvents.RemoveListener<Game.Event.GameEnded>(OnGameEnded);
        }

        public void Update()
        {
            text.text = Time.timeSinceLevelLoad.ToString("####0.00");
        }

        #region Event Handlers

        public void OnGameEnded(Game.Event.GameEnded ev)
        {
            enabled = false;
        }

        #endregion
    }

}
