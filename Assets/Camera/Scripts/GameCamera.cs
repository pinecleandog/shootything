﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class GameCamera : MonoBehaviour
{
    new private Camera camera;
    private Plane viewPlane;

    public void Awake()
    {
        camera = GetComponent<Camera>();
        if(camera == null)
        {
            Debug.LogError("No Camera Component attached to GameCamera");
            enabled = false;
        }

        viewPlane.SetNormalAndPosition(
            camera.transform.forward,
            Vector3.down);
    }

    /// <summary>
    /// Returns if a world space point is within the screen bounds
    /// </summary>
    /// <param name="worldPoint">A point in the world</param>
    /// <returns></returns>
    public bool PointWithinScreen(Vector3 worldPoint)
    {
        Vector3 viewPoint = camera.WorldToViewportPoint(worldPoint);

        return viewPoint.x >= 0 && viewPoint.x <= 1 && viewPoint.y >= 0
            && viewPoint.y <= 1;
    }

    /// <summary>
    /// Gets a corrected world point from a screen position, on the correct 
    /// viewing plane of the camera
    /// </summary>
    /// <param name="screenPoint">Point in the screen's space</param>
    /// <returns>A world space position</returns>
    public Vector3 screenToWorldPoint(Vector2 screenPoint)
    {
        Ray ray = camera.ScreenPointToRay(screenPoint);
        float dist;
        viewPlane.Raycast(ray, out dist);

        return ray.GetPoint(dist);
    }
}
