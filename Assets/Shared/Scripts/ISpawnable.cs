﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISpawnable
{ 
    Transform Transform { get; }
    ISpawner Spawner { get; set; }
    // Set to a value greater than zero if they are to automatically expire
    float ExpireyTime { get; set; }
    void Enable();
    void Disable();
    void Destroy();
}
