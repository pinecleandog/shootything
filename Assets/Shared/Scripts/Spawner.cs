﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface ISpawner
{
    void DestroyObject(ISpawnable spawnable);
}

public class Spawner<T> : MonoBehaviour, ISpawner where T : ISpawnable
{
    [SerializeField]
    protected GameObject prefab;

    [SerializeField, Tooltip("Number of projectiles to fire per second")]
    protected int objectSpawnRate = 5;

    [SerializeField, Tooltip("Duration of object lifetime")]
    protected float objectLifeTime = 2f;

    protected delegate void ObjectSpawn(T obj);
    protected ObjectSpawn onObjectSpawn;
 
    // tracking objects that have an expirey
    protected List<T> tracked = new List<T>();
    protected Queue<T> pool = new Queue<T>();

    protected bool doSpawn = false;

    private float nextSpawnTime = 0;

    public virtual void Awake()
    {
        if(prefab == null)
        {
            Debug.LogError("Prefab is null on " + GetType());
            enabled = false;
        }

        if (objectSpawnRate <= 0)
        {
            objectSpawnRate = 1;
        }

        doSpawn = true;
    }

    public virtual void Update()
    {
        if (!doSpawn) return;

        if (Time.time >= nextSpawnTime)
        {
            if (pool.Count < 1)
            {
                pool.Enqueue(createObject());
            }

            var spawnable = pool.Dequeue();
            onObjectSpawn.Invoke(spawnable);
            spawnable.ExpireyTime = Time.time + objectLifeTime;
            spawnable.Enable();

            if (objectLifeTime >= 0)
            {
                tracked.Add(spawnable);
            }

            if (objectSpawnRate <= 0) return;
            nextSpawnTime = Time.time + (1f / objectSpawnRate);
        }

        disableExpiredObjects();
    }

    public void DestroyObject(ISpawnable spawnable)
    {
        spawnable.Disable();

        T obj;

        try
        {
            obj = (T)spawnable;
        } catch (System.InvalidCastException e)
        {
            Debug.Log("Tried to destroy an object that's not of the correct type");
            return;
        }

        if(tracked.Contains(obj))
        {
            tracked.Remove(obj);
            pool.Enqueue(obj);
        }
    }

    private void disableExpiredObjects()
    {
        // projectiles are added in order of time spawned, so 
        // further projectiles wont be expired
        while (tracked.Count > 0 && Time.time >= tracked[0].ExpireyTime)
        {
            var projectile = tracked[0];
            tracked.RemoveAt(0);
            pool.Enqueue(projectile);
        }
    }

    protected void populatePool(int amt)
    {
        for (int i = 0; i < amt; i++)
        {
            var obj = createObject();
            pool.Enqueue(obj);
        }
    }

    private T createObject()
    {
        GameObject obj = Instantiate(
                prefab,
                Vector3.zero,
                Quaternion.identity);

        var spawnable = obj.GetComponent<T>();
        if (spawnable == null)
        {
            Debug.LogError("Missing " + typeof(T).ToString() + " implementation on Spawner");
            return spawnable;
        }

        spawnable.Spawner = this;
        spawnable.Disable();      

        return spawnable;
    }
}
