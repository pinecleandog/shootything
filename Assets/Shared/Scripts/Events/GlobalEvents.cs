﻿
namespace Events
{
    /// <summary>
    /// A wrapper for a single event stream, which is globally available to all classes
    /// via a set of static methods
    /// </summary>
    public class GlobalEvents
    {
        #region Private Fields/Properties

        private static GlobalEvents Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GlobalEvents();
                }

                return instance;
            }
        }
        private static GlobalEvents instance = null;

        private EventStream eventStream;

        #endregion

        /// <summary>
        /// Creates a global events object
        /// </summary>
        public GlobalEvents()
        {
            eventStream = new EventStream();
        }
        /// <summary>
        /// Creates an event object, or retrieves one from the event cache
        /// </summary>
        /// <typeparam name="T">Type of event</typeparam>
        /// <returns>An event object</returns>
        public static T CreateEventObject<T>() where T : Event, new()
        {
            return Instance.eventStream.CreateEventObject<T>();
        }
        /// <summary>
        /// Adds an event listener for a global event
        /// </summary>
        /// <typeparam name="T">The type of event the listener is listening for</typeparam>
        /// <param name="handler">The method that handles the event</param>
        public static void AddListener<T>(EventHandler<T> handler) where T : Event
        {
            Instance.eventStream.AddListener<T>(handler);
        }
        /// <summary>
        /// Removes an event listener for a global event
        /// </summary>
        /// <typeparam name="T">The type of event</typeparam>
        /// <param name="handler">The method that handles the event to be removed</param>
        public static void RemoveListener<T>(EventHandler<T> handler) where T : Event
        {
            Instance.eventStream.RemoveListener<T>(handler);
        }
        /// <summary>
        /// Triggers a global event/message with event data
        /// </summary>
        /// <param name="ev">The event to trigger</param>
        public static void Trigger<T>(T ev) where T : Event, new()
        {
            Instance.eventStream.Trigger(ev);
        }
    }

}
