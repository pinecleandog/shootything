﻿namespace Events
{
    /// <summary>
    /// An interface for an event used in EventStreams
    /// </summary>
	public abstract class Event
	{
        public Event() { }
        public abstract void Reset();
        public EventStream Stream = null;
	}
}