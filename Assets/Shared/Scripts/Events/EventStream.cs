﻿using System;
using System.Collections.Generic;

namespace Events
{
	/// <summary>
	/// A delegate for an event handler
	/// </summary>
	/// <typeparam name="T">The type of event</typeparam>
	/// <param name="ev">The event data passed back to the handler</param>
	public delegate void EventHandler<T>(T ev) where T : Event;

	/// <summary>
	/// An type safe event stream, that can be used for transmitting messages with data
	/// </summary>
	public class EventStream
	{
        #region Private Fields/Properties

        private delegate void EventHandler(Event ev);

		private Dictionary<Type, EventHandler> handlers = new Dictionary<Type, EventHandler>();
		private Dictionary<Delegate, EventHandler> handlerLookup = new Dictionary<Delegate, EventHandler>();
        private EventCache eventCache = new EventCache();

        #endregion

        #region Public Methods

        /// <summary>
        /// Creates an event object, or retrieves one from the event cache
        /// </summary>
        /// <typeparam name="T">Type of event</typeparam>
        /// <returns>An event object</returns>
        public T CreateEventObject<T>() where T : Event, new()
        {
            var obj = eventCache.RetrieveObject<T>();

            if(obj == null)
            {
                obj = new T();
            }
            else
            {
                obj.Reset();
            }

            obj.Stream = this;

            return obj;
        }
        /// <summary>
        /// Adds a listener for specific type of event that's triggered on this stream
        /// </summary>
        /// <typeparam name="T">The type of event</typeparam>
        /// <param name="handler">A method that will handle the event</param>
        public void AddListener<T>(EventHandler<T> handler) where T : Event
		{
			if (handlerLookup.ContainsKey(handler)) return;

			EventHandler internalHandler = (ev) => handler((T)ev);
			handlerLookup.Add(handler, internalHandler);

			EventHandler tempHandler;
			if (handlers.TryGetValue(typeof(T), out tempHandler))
			{
				handlers[typeof(T)] = tempHandler += internalHandler;
			}
			else
			{
				handlers[typeof(T)] = internalHandler;
			}
		}
		/// <summary>
		/// Removes an event handler for a specific type of event
		/// </summary>
		/// <typeparam name="T">The type of event</typeparam>
		/// <param name="handler">The method that handles the event</param>
		public void RemoveListener<T>(EventHandler<T> handler) where T : Event
		{
			EventHandler internalHandler;
			if (handlerLookup.TryGetValue(handler, out internalHandler))
			{
				EventHandler tempHandler;
				if (handlers.TryGetValue(typeof(T), out tempHandler))
				{
					tempHandler -= internalHandler;
					if (tempHandler == null)
					{
						handlers.Remove(typeof(T));
					}
					else
					{
						handlers[typeof(T)] = tempHandler;
					}
				}

				handlerLookup.Remove(handler);
			}
		}

		/// <summary>
		/// Triggers an event object, that all handlers will receive with data
		/// </summary>
		/// <param name="ev">The event/message to trigger with data</param>
		public void Trigger<T>(T ev) where T : Event, new()
		{
			if (ev == null) return;

            if(ev.Stream != this)
            {
                UnityEngine.Debug.LogError("Tried to trigger an event that does not belong to this stream. " +
                    "The event was probably not created using CreateEventObject()");
                return;
            }

			EventHandler handler;
			if (handlers.TryGetValue(ev.GetType(), out handler))
			{
				handler.Invoke(ev);
			}

            eventCache.CacheObject(ev);
		}

        #endregion
    }
}