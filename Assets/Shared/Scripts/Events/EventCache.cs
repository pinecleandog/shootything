﻿using System.Collections.Generic;

namespace Events
{
    public class EventCache
    {
        private Dictionary<System.Type, Queue<object>> cache 
            = new Dictionary<System.Type, Queue<object>>();

        public void CacheObject<T>(T obj) where T : Event, new()
        {
            System.Type type = typeof(T);
            if(!cache.ContainsKey(type))
            {
                cache.Add(type, new Queue<object>());
            }

            cache[type].Enqueue(obj);
        }

        public T RetrieveObject<T>() where T : Event, new()
        {
            System.Type type = typeof(T);

            if (!cache.ContainsKey(type) || cache[type].Count == 0)
            {
                return null;
            }

            return cache[type].Dequeue() as T;
        }
    }
}

