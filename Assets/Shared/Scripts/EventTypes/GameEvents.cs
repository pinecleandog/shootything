﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;

namespace Game.Event
{
    public class PlayerShipDestroyed : Events.Event
    {
        public Transform Ship { get; set; }

        public override void Reset()
        {
            Ship = null;
        }
    }

    public class ObstacleDestroyed : Events.Event
    {
        public Obstacle Obstacle { get; set; }

        public override void Reset()
        {
            Obstacle = null;
        }
    }

    public class GameEnded : Events.Event
    {
        public override void Reset()
        {
            
        }
    }
}