﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;

public class GameManager : MonoBehaviour
{
    [SerializeField, Tooltip("Delay before resetting the game, once ended")]
    private float endGameDelay = 2f;

    private float reloadSceneTime = -1;

    private static GameManager instance = null;

    public static GameManager Instance
    {
        get
        {
            if(instance == null)
            {
                createInstance();
            }

            return instance;
        }
    }

    public void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void OnEnable()
    {
        GlobalEvents.AddListener<Game.Event.PlayerShipDestroyed>(OnPlayerShipDestroyed);
    }

    public void OnDisable()
    {
        GlobalEvents.RemoveListener<Game.Event.PlayerShipDestroyed>(OnPlayerShipDestroyed);
    }

    public void Update()
    {
        if(reloadSceneTime >= 0 && Time.time > reloadSceneTime)
        {
            reloadSceneTime = -1;
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(0);
        }
    }

    public void EndGame()
    {
        reloadSceneTime = Time.time + endGameDelay;

        var ev = GlobalEvents.CreateEventObject<Game.Event.GameEnded>();
        GlobalEvents.Trigger(ev);
    }

    public void OnApplicationQuit()
    {
        instance = null;
    }

    private static void createInstance()
    {
        instance = FindObjectOfType(typeof(GameManager)) as GameManager;

        if(instance == null)
        {
            var obj = new GameObject();
            instance = obj.AddComponent<GameManager>();
            instance.transform.name = "GameManager";
            instance.Awake();
        }
    }

    #region Event Handlers

    public void OnPlayerShipDestroyed(Game.Event.PlayerShipDestroyed ev)
    {
        EndGame();
    }

    #endregion
}
